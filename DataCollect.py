import pandas
import matplotlib.pyplot as plt

data = pandas.DataFrame.from_csv("C:/Experiments/FinalTest.csv", sep = ', ')

data = data.iloc[::2]

plt.plot(data.Attention, label = 'Attention')
plt.plot(data.Meditation, label = 'Meditation')
plt.legend()
plt.show()
